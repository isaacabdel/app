angular.module('slyde.settings', ['ionic'])

.controller('settings', function($scope, $ionicModal, $cordovaDialogs) {

  $ionicModal.fromTemplateUrl('app/settings/settings.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalSettings = modal;
  });

  $scope.openSettingsModal = function() {
    $scope.modalSettings.show();
  };
  $scope.closeSettingsModal = function() {
    $scope.modalSettings.hide();
  };

  // search for friend
  $scope.findFriend = function() {
    // prompt user to enter a username
    $cordovaDialogs.prompt('', 'Enter username to follow', ['Follow', 'Cancel'], '')
      .then(function(result) {
        var input = result.input1;
        // no button = 0, 'Follow' = 1, 'Cancel' = 2
        var btnIndex = result.buttonIndex;

        if (3) { // prevent current user from following themselves

        } else if (3) { // check if username entered exists

        } else { // username does not exist
          $cordovaDialogs.alert('', 'No user by that name', 'Okay')
            .then(function() {
              // callback success
            });
        }

      });

  }

  // log out
  $scope.logout = function() {
    $state.go('welcome', {
      clear: true
    });
  }

})
