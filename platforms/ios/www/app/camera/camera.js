angular.module('slyde.camera', ['ionic'])

.controller('camera', function($scope, $ionicModal, $cordovaCamera) {

  $ionicModal.fromTemplateUrl('app/camera/camera.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalCamera = modal;
  });

  $scope.openCameraModal = function() {
    $scope.modalCamera.show();
  };
  $scope.closeCameraModal = function() {
    $scope.modalCamera.hide();
  };

  $scope.takePicture = function() {
    var options = {
      quality: 50,
      destinationType: Camera.DestinationType.DATA_URL,
      sourceType: Camera.PictureSourceType.CAMERA,
      allowEdit: true,
      encodingType: Camera.EncodingType.JPEG,
      targetWidth: 450,
      targetHeight: 600,
      popoverOptions: CameraPopoverOptions,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    $cordovaCamera.getPicture(options).then(function(imageData) {
      $scope.image = "data:image/jpeg;base64," + imageData;

      $scope.uploadPicture = function() {
        // show uploading overlay
        $ionicLoading.show({
          template: '<ion-spinner icon="android" class="spinner-balanced"></ion-spinner> <br> Uploading...'
        });

        // upload picture

        // delete old slydes

        // hive uploading overlay
        $ionicLoading.hide()
      }

    }, function(err) {
      // error
    });
  }

})
