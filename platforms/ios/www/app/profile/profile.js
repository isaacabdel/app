angular.module('slyde.profile', ['ionic'])

.controller('profile', function($scope, $ionicModal, $ionicActionSheet, $cordovaDialogs) {

  $ionicModal.fromTemplateUrl('app/profile/profile.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalProfile = modal;
  });

  $scope.openProfileModal = function() {
    $scope.modalProfile.show();
  };
  $scope.closeProfileModal = function() {
    $scope.modalProfile.hide();
  };

  // open action sheet
  // contains user options
  $scope.openUserOptions = function() {
    // Show the action sheet
    var hideSheet = $ionicActionSheet.show({
      destructiveText: 'Report Post',
      cancelText: 'Cancel',
      cancel: function() {
        //
      },
      destructiveButtonClicked: function() {
        $cordovaDialogs.confirm('This post will be reported as inappropriate', 'Report ', ['Confirm', 'Cancel'])
          .then(function(buttonIndex) {
            // no button = 0, 'Report' = 1, 'Cancel' = 2
            var btnIndex = buttonIndex;
            if (btnIndex == 1) {
              // report post

              hideSheet();
            }
          });
      }

    });
  }


})
