angular.module('slyde.welcome', ['ionic'])

.controller('welcome', function($scope, $ionicModal, $ionicLoading, $cordovaDialogs) {

  // log in modal
  $ionicModal.fromTemplateUrl('app/welcome/login.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalLogIn = modal;
  });

  $scope.openLogInModal = function() {
    $scope.modalLogIn.show();
    // $scope.null();
  };
  $scope.closeLogInModal = function() {
    $scope.modalLogIn.hide();
    $scope.usernull();
  };

  // sign up modal
  $ionicModal.fromTemplateUrl('app/welcome/signup.html', {
    scope: $scope,
    animation: 'slide-in-up'
  }).then(function(modal) {
    $scope.modalSignUp = modal;
  });

  $scope.openSignUpModal = function() {
    $scope.modalSignUp.show();
  };
  $scope.closeSignUpModal = function() {
    $scope.modalSignUp.hide();
  };

  // loading overlay
  $scope.showLoading = function() {
    $ionicLoading.show({
      template: '<ion-spinner icon="android" class="spinner-balanced"></ion-spinner> <br> Loading...'
    });
  }
  $scope.hideLoading = function() {
    $ionicLoading.hide();
  }

  // User
  $scope.usernull = function() { // set all user object to null
    $scope.user = {
      username: null,
      password: null,
      email: null,
    };
  }
  $scope.usernull();

  // log in
  $scope.login = function() {
    $scope.showLoading(); // show loading overlay
    $state.go('home', {
      clear: true
    });
  }


  // sign up
  $scope.signup = function() {
    $scope.showLoading(); // show loading overlay

    var user = $scope.user

    var passwordPattern = new RegExp(/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?_]/); // only allow numbers and letters
    var usernamePattern = new RegExp(/[~`!#$%\^&*+=\-\[\]\\';,/{}|\\":<>\?]/); // only allow numbers, letters, and underscores

    // check if username is longer than 3 characters
    if (user.username.length < 3) {
      $scope.hideLoading();
      console.log("username is too short");
    }
    // check if password is longer than 6 characters
    else if (user.password.length < 6) {
      $scope.hideLoading();
      console.log("password is too short");
    }
    // check for special characters in username
    // * username should only contain numbers, letters, and underscores
    else if (usernamePattern.test(user.username)) {
      $scope.hideLoading();
      console.log("not valid username");
    }
    // check for special characters in password
    // * password should only contain numbers and letters
    else if (passwordPattern.test(user.password)) {
      $scope.hideLoading();
      console.log("not valid password");
    } else {
      $scope.hideLoading();
      console.log("passed");

      // sign up user
    }


  }

  // display info on the importance of email
  $scope.showInfoAboutEmail = function() {
    $cordovaDialogs.alert('You will need your email to log in, make sure its correct.', 'Important', 'Got It!')
      .then(function() {
        // callback success
      });
  }



})
